# Starter bot for Zombie Wars

### Node.<span></span>js

Download and install [Node.js v10.9.0](https://nodejs.org/en/). To run the bot,

    $ node index.js

### NW.<span></span>js

Download and extract [NW.js v0.32.4](https://nwjs.io/), extract in to the folder and run `nw.exe`.
