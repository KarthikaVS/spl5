/*
 * bot.js
 *
 * Main logic of the bot resides in this file.
 *
 * This Bot class recieves an incoming request, look at the
 * data, figures out the response and then send it back.
 *
 */

const SplClient = require('./splClient.js');

class Bot {
    

    constructor(host, port) {
        this.splClient = new SplClient(host, port, this.request.bind(this));
        this.opponentId;
        this.isInitDone = false;
        this.allPositions = [];
        this.attackStarted = false;
        console.log('asdjasdfbasbdfuagsusudayfgsuodagsuagdfusdagf');
    }

    start() {
        console.log('listen function');
        this.splClient.listen();
    }

    /*
     * Entry point for each incoming request from SBOX
     *
     */
    request(data) {
        // try {
            
            switch(data.dataType.trim()) {
                case "authentication" :
                    this.authentication(data);
                    break;
                case "command" :
                    this.move(data);
                    break;
                case "acknowledge" :
                    this.acknowledgement(data);
                    break;
                case "result" :
                    this.isInitDone = false;
                    this.attackStarted = false;
                    this.allPositions = [];
                    this.result(data);
                    break;
            }
        // } 
        // catch (err) {
        //     console.error("Error processing request");
        //     console.error(err);
        // }
    }

    authentication(data) {
        // Send back the one-time-password that
        // was received in the request.
        let response = {
            dataType: 'oneTimePassword',
            oneTimePassword: data.oneTimePassword,
        };
        this.splClient.respond(response);
    }

    acknowledgement(data) {
        console.log("Ack :: status :", data.message);
    }

    result(data) {
        console.log("Game over ::", data.result);
    }

    getPositions(request){
        let myStatus = [];
        request.boardInfo.forEach((rowArray, rowIndex) => {
            rowArray.forEach((colEle, colIndex) => {
                if (colEle === request.yourID) {
                    let obj = {};
                    obj.row = rowIndex;
                    obj.col = colIndex;
                    obj.movablePositions = this.canIMove(request.boardInfo, rowIndex, colIndex, 'move');
                    obj.jumpablePositions = this.canIMove(request.boardInfo, rowIndex, colIndex, 'jump');
                    myStatus.push(obj);
                } 
            });
        });
        return myStatus;
    }

    findClosestCornerDistance(pos, boardInfo){
        if((pos.row === 0 &&  pos.col === 0) || (pos.row === boardInfo.length - 1 &&  pos.col === 0) || (pos.row === 0 &&  pos.col === boardInfo.length -1) || (pos.row === boardInfo.length -1 &&  pos.col === boardInfo.length -1)){
            return {
                dir:"corner",
                dist: 0
            }
        }
        if(pos.col < boardInfo/2){
            return {
                dir:"left",
                dist: pos.col - 1
            };
        }else{
            return {
                dir: "right",
                dist: (boardInfo.length - 1 - pos.col)
            }
        }
    }

    move(request) {
        this.opponentId = request.yourID === 1 ? 2 : 1;
        let myStatus = [];
        myStatus = this.getPositions(request);

        console.log('My status : ', myStatus);

        let result = this.deploy(request.boardInfo, myStatus);
        //
        // ** Move logic here. **
        //
        let response = {
            dataType: "response",
            fromCell: [result.fromCell.row, result.fromCell.col],
            toCell: [result.toCell.row, result.toCell.col],
        };

        this.splClient.respond(response);
    }

    pushToPreferredColumns(value){
        if(value >= 0 && value < boardInfo.length){
            this.preferredColumns.push(value);
            return true;
        }
        return false;
    }

    pushToPreferredRows(value){
        if(value >= 0 && value < boardInfo.length){
            this.preferredRows.push(value);
            return true;
        }
        return false;
    }

    isAvailable(movePos) {
        let isThere = false;
        this.allPositions.forEach(pos => {
            // console.log('(',pos.movePos.row , movePos.row ,')' ,'(', pos.movePos.col , movePos.col);
            if (pos.movePos.row === movePos.row && pos.movePos.col === movePos.col) {
                // console.log('It is already there : ', JSON.stringify(this.allPositions));
                isThere = true;
            }
        });
        return isThere;
    }

    getElement(boardInfo){
        let ele = this.allPositions.shift();
        if (!ele) {
            return false;
        }
        // console.log(boardInfo[ele.fromCell.row][ele.fromCell.col], boardInfo[ele.movePos.row][ele.movePos.col])
        if (boardInfo[ele.movePos.row][ele.movePos.col] !== 0 || (this.attackStarted && boardInfo[ele.fromCell.row][ele.fromCell.col] !== 0)) {
            this.getElement(boardInfo);
        } else {
            return ele;
        }
    }

    deploy(boardInfo, myStatus) {
        var previousCount = 0, move;
        myStatus.forEach(eachPos => {

            if (this.attackStarted) {
                eachPos.jumpablePositions.forEach(movePos => {
                    let unjumpcount = this.findOpponent(boardInfo, movePos, "move") + this.findOpponent(boardInfo, {row: eachPos.row, col:eachPos.col}, "jump"); 
                    let jumpcount = this.findOpponent(boardInfo, {row: eachPos.row, col:eachPos.col}, "jump");
                    // console.log('jump count : ', jumpcount);
                    if (!this.isAvailable(movePos)) {
                        this.allPositions.push({fromCell: {row: eachPos.row, col: eachPos.col}, movePos: movePos});
                    }
                    if (jumpcount  === 0 && unjumpcount > 0) {
                        previousCount = unjumpcount;
                        // this.removeFromAllPosition(ele);
                        move = {
                            fromCell: {row: eachPos.row, col: eachPos.col},
                            toCell: movePos
                        };
                    }
                });  
            }

            eachPos.movablePositions.forEach(movePos => {
                let oppocount = this.findOpponent(boardInfo, movePos, "move");
                // console.log('oppocount: ', oppocount);
                if (!this.isAvailable(movePos)) {
                    this.allPositions.push({fromCell: {row: eachPos.row, col: eachPos.col}, movePos: movePos});
                }
                if (oppocount > 0) {
                    this.attackStarted = true;
                }
                if (previousCount < oppocount) {
                    previousCount = oppocount;
                    move = {
                        fromCell: {row: eachPos.row, col: eachPos.col},
                        toCell: movePos
                    };
                }
            });
           
        });
        if(previousCount === 0){
            // console.log('\n\nBEFORE SHIFTING :',JSON.stringify(this.allPositions), '\n\n');
            // var ele = this.allPositions.shift();
            // if (boardInfo[ele.fromCell.row][ele.fromCell.col] !== 0) {
                
            // }
            let ele = this.getElement(boardInfo);
            // console.log('\n\n AFTER SHIFTING :',JSON.stringify(this.allPositions), '\n\n');
            // console.log('ELE : ', ele);
            if (!ele) {
                // var fromCell, toCell;
                myStatus.forEach(myPosition => {
                    if (myPosition.movablePositions.length > 0) {
                        myPosition.fromCell = {row: myPosition.row, col: myPosition.col};
                        myPosition.toCell = {row: myPosition.movablePositions[0].row, col: myPosition.movablePositions[0].col};
                    } else if (myPosition.jumpablePositions.length > 0) {
                        myPosition.fromCell = {row: myPosition.row, col: myPosition.col};
                        myPosition.toCell = {row: myPosition.jumpablePositions[0].row, col: myPosition.jumpablePositions[0].col};
                    }
                });
                var decisionIndex = this.getRandomNumber(myStatus, 0);
                ele = {
                    fromCell: {row: myStatus[decisionIndex].fromCell.row, col: myStatus[decisionIndex].fromCell.col},
                    movePos: {row: myStatus[decisionIndex].toCell.row, col: myStatus[decisionIndex].toCell.col},
                }
            }
            move = {
                fromCell: {row: ele.fromCell.row, col: ele.fromCell.col},
                toCell: {row: ele.movePos.row, col: ele.movePos.col},
            };
            // console.log('Move at random : ', JSON.stringify(move));
        } 
        this.removeFromAllPosition(move.toCell);

        return move;
    }
    removeFromAllPosition(obj) {
        this.allPositions.forEach((position, index) => {
            if (
                position.movePos.row === obj.row &&
                position.movePos.col === obj.col
            ) {
                this.allPositions.splice(index, 1);
                // console.log('spliced : ', position, index);
            }
        })
    }
    getRandomNumber(myStatus, index){
        // console.log(index);
        if(myStatus[index].fromCell && index < myStatus.length){
            // console.log('have from cell', index)
            return index;
        }else{
            // console.log('no from cell')
            return this.getRandomNumber(myStatus, ++index);
        }
    }
    findOpponent(boardInfo, movePos, status) {
        var row, col;
        var rowPos = movePos.row, colPos = movePos.col, count = 0, square;
        if (status === "move") {
            square = 1;
        } else if (status === "jump") {
            square = 2;
        }
        
        // left to right
        row = rowPos - square, col = colPos - square;
        var obj = this.isInRange(boardInfo, row, col, status, 'leftToRight');
        if (obj) {
            row = obj.row, col = obj.col;
            for (; col <= colPos + square && col < boardInfo.length; col++) {
                if (boardInfo[row][col] === this.opponentId) {
                    count++;
                }
            }
        }else{
            row = -1;
            col = colPos + 3;
        }
        // console.log('left to right obj =', obj);
        // top rigt to down
        row = row + 1, col = col - 1;
        obj = this.isInRange(boardInfo, row, col, status, 'topToDown');
        if (obj) {
            row = obj.row, col = obj.col;
            for (; row <= rowPos + square && row < boardInfo.length; row++ ) {
                if (boardInfo[row][col] === this.opponentId) {
                    count++;
                }
            }
        }else{
            row = rowPos + 3;
            col = boardInfo.length;
        }
        // console.log('top to down obj = ', obj)
       
        // right to left
        row = row - 1, col = col - 1;
        obj = this.isInRange(boardInfo, row, col, status, 'rightToLeft');
        if (obj) {
            row = obj.row, col = obj.col;
            for (; col >= colPos - square && col >= 0; col-- ) {
                if (boardInfo[row][col] === this.opponentId) {
                    count++;
                }
            }                                             
        }else{
            row = boardInfo.length;
            col = colPos - 3;
        }
        // console.log('right to left obj = ', obj)
        //left to top
        row = row - 1, col = col + 1;
        obj = this.isInRange(boardInfo, row, col, status, 'downToTop');
        if (obj) {
            row = obj.row, col = obj.col;
            for (; row > rowPos - square && row >= 0; row--) {
                if (boardInfo[row][col] === this.opponentId) {
                    count++;
                }
            }
        }
        // console.log('left to top obj = ', obj)
        return count;
    }

    canIMove(boardInfo, rowPos, colPos, status) {
        // Here row pos & col pos will define my index
        var square;
        if (status === 'move') {
            square = 1;
        } else if (status === 'jump') {
            square = 2;
        }
        
        var movablePositions = [];
        var row, col;
        // console.log('rowps = ', rowPos);
        // console.log('colpos =', colPos)
        // left to right
        row = rowPos - square, col = colPos - square;
        var obj = this.isInRange(boardInfo, row, col, status, 'leftToRight');
        // console.log('left to right obj = ', obj);
        if (obj) {
            row = obj.row, col = obj.col;
            for (; col <= colPos + square && col < boardInfo.length; col++) {
                if (boardInfo[row][col] === 0) {
                    movablePositions.push({row: row, col: col});
                }
            }
        }else{
            row = -1;
            col = colPos + 3;
        }

        // top rigt to down
        // row = rowPos, col = colPos + square;
        row = row + 1, col = col - 1;
        obj = this.isInRange(boardInfo, row, col, status, 'topToDown');
        // console.log('toptodown obj = ', obj)
        if (obj) {
            row = obj.row, col = obj.col;
            for (; row <= rowPos + square && row < boardInfo.length; row++ ) {
                if (boardInfo[row][col] === 0) {
                    movablePositions.push({row: row, col: col});
                }
            }
        }else{
            row = rowPos + 3;
            col = boardInfo.length;
        }
       
        // right to left
        row = row - 1, col = col - 1;
        obj = this.isInRange(boardInfo, row, col, status, 'rightToLeft');
        // console.log('right to left oj = ', obj)
        if (obj) {
            row = obj.row, col = obj.col;
            for (; col >= colPos - square && col >= 0; col-- ) {
                if (boardInfo[row][col] === 0) {
                    movablePositions.push({row: row, col: col});
                }
            }                                             
        }else{
            row = boardInfo.length;
            col = colPos - 3;
        }

        //left to top
        row = row - 1, col = col + 1;
        obj = this.isInRange(boardInfo, row, col, status, 'downToTop');
        // console.log('down to top obj =', obj)
        if (obj) {
            row = obj.row, col = obj.col;
            for (; row > rowPos - square && row >= 0; row--) {
                if (boardInfo[row][col] === 0) {
                    movablePositions.push({row: row, col: col});
                }
            }
        }
        return movablePositions;
    }

    isInRange(boardInfo, row, col, status,  direction) {
        // let canContinue = true;
        if (status === 'move') {
            if ( row < 0 ) {
                row = 0;
            } 
            if ( row >= boardInfo.length ) {
                row = boardInfo.length - 1;
            }
            if ( col < 0 ) {
                col = 0;
            }
            if ( col >= boardInfo.length ) {
                col = boardInfo.length -1;
            }
        
        }else{
            if(direction === 'leftToRight'){
                if(row < 0){
                    return false;
                }
                if(col < 0){
                    col = 0;
                }
            }else if(direction === 'topToDown'){
                if(col > boardInfo.length - 1){
                    return false;
                }
                if(row < 0){
                    row = 0;
                }
            } else if(direction === 'rightToLeft'){
                if(row > boardInfo.length - 1){
                    return false;
                }
                if(col > boardInfo.length - 1){
                    col = boardInfo.length - 1;
                }
            } else if(direction === 'downToTop'){
                if(col < 0){
                    return false;
                }
                if(row < 0){
                    row = 0;
                }
            }
        }  


        
        // if (canContinue) {
            return {row: row, col: col};
        // } else {
        //     return false;
        // }
    }
}

module.exports = Bot;
